# 1

library(DBI)
source(file = "connexion_db.R")
conn<-connecter()
DBI::dbListTables(conn)


boulodromes_de_france <- st_read(dsn = conn, query = "SELECT * FROM bpe21_metro WHERE TYPEQU='F102'")

region_paca_de_france <- st_read(dsn = conn, query = "SELECT * FROM regions_metro WHERE code='93'")

# 2 et 3 à peu près

boulodromes_de_paca_de_france_du_sud <- st_read(dsn = conn, query = "SELECT * FROM bpe21_metro WHERE TYPEQU='F102' AND (dep='06' OR dep='83' OR dep='13' OR dep='04' OR dep='84' OR dep='05')")