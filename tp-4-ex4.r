# 1a

metro <- st_read("fonds/commune_francemetro_2021.shp",
                    options="ENCODING=WINDOWS-1252")

# 1b

sport <- read.csv2("bpe20_sport_loisir_xy.csv", sep=";", dec=".")

str(sport)

# 2

map <- leaflet() %>% 
  setView(lng = 2.351, lat = 46.77, zoom = 5) %>% 
  addTiles()

map

# 3a

BOWLINGS <- sport %>% 
  filter(REG >10) %>% 
  filter(TYPEQU == "F119") %>% 
  filter(!is.na(LAMBERT_X))

# 3b

BOWLINGS_sf <- st_as_sf(BOWLINGS, coords=c(10,11), crs=2154)
BOWLINGS_sf <- st_transform(BOWLINGS_sf, crs = 4326)

# 3c, 3d

addMarkers(data = BOWLINGS_sf, map = map, popup = BOWLINGS_sf$DEPCOM)

# 4a

map <- leaflet() %>% 
  setView(lng = 2.2501, lat = 48.8306, zoom = 11) %>% 
  addTiles()

map

# 4b


pop <- read.csv2("base-cc-evol-struct-pop-2018_echantillon.csv", sep=",", dec=".")

pop92 <- pop %>% 
  select(CODGEO, P18_POP, P18_POP0014) %>% 
  filter(substr(CODGEO, 1, 2) ==92)

# 4c


