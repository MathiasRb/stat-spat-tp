# install.packages("osrm")
library(osrm)

library(DBI)
source(file = "connexion_db.R")
conn<-connecter()
DBI::dbListTables(conn)

# Ici, on prend pour exemple les pompes_funnités
pompes_fun <- sf::st_read(conn, query = "SELECT * FROM bpe21_metro WHERE TYPEQU='A205' AND DEPCOM='35047';") %>%
  slice(1)

# On récupère ses coordonnées 
pompes_fun_coords <- st_coordinates(pompes_fun) %>% as.numeric

sf_reg_metro <- st_read(conn, query = "SELECT * FROM regions_metro")
plot(st_geometry(sf_reg_metro))
points(x = pompes_fun_coords[1], y = pompes_fun_coords[2], pch = 4, lwd = 2, cex = 1.5, col = "red")

# On transforme ses coordonnées en WGS84 (epsg=4326)
pompes_fun_coords <- st_coordinates(pompes_fun %>% st_transform(4326)) %>% as.numeric

leaflet() %>% 
  setView(lng = pompes_fun_coords[1], lat = pompes_fun_coords[2], zoom = 14) %>% 
  addTiles() %>% 
  addMarkers(lng = pompes_fun_coords[1], lat = pompes_fun_coords[2])


# Attention, cela peut prendre quelques minutes
iso <- osrm::osrmIsochrone(
  loc = pompes_fun_coords, # coordonnées du point de référence
  breaks = seq(0,60,10), # valeurs des isochrones à calculer en minutes
  res = 100 # détermine le nombre de points utilisés (res*res) pour dessiner les isochornes 
)
str(iso)

bks <-  sort(unique(c(iso$isomin, iso$isomax)))
pals <- hcl.colors(n = length(bks) - 1, palette = "Red-Blue", rev = TRUE)
plot(iso["isomax"], breaks = bks, pal = pals, 
     main = "Isochrones (in minutes)", reset = FALSE)
points(x = pompes_fun_coords[1], y = pompes_fun_coords[2], pch = 4, lwd = 2, cex = 1.5)

leaflet() %>% 
  setView(lng = pompes_fun_coords[1], lat = pompes_fun_coords[2], zoom = 8) %>% 
  addTiles() %>% 
  addMarkers(lng = pompes_fun_coords[1], lat = pompes_fun_coords[2]) %>% 
  addProviderTiles(
    providers$CartoDB.DarkMatter,
    options = providerTileOptions(opacity = 0.4)) %>%
  addPolygons(
    data=iso, 
    fillColor = pals,
    smoothFactor = 0.3,
    weight = 1,
    opacity = 1,
    color = "white",
    dashArray = "3",
    fillOpacity = 0.65
  ) %>% 
  addLegend(
    position="bottomleft",
    colors=pals,
    labels=rev(c("50-60","40-50",
                 "30-40","20-30","10-20", "0-10")),
    opacity = 0.6,
    title="Temps de trajet par la route (en minutes)")
