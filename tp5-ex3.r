# 1

library(DBI)
source(file = "connexion_db.R")
conn<-connecter()
DBI::dbListTables(conn)

bpe_dep50 <- dbSendQuery(conn = conn, statement = "SELECT ID, DEPCOM, DOM, SDOM, TYPEQU, GEOMETRY FROM bpe21_metro WHERE DEP = '50'")

# 2

bpe_dep50 <- st_read(dsn = conn, query = "SELECT ID, DEPCOM, DOM, SDOM, TYPEQU, GEOMETRY FROM bpe21_metro WHERE DEP = '50'")

# 3

str(bpe_dep50)
dbGetQuery(conn, "SELECT DISTINCT(ST_SRID(geometry)) FROM bpe21_04;")

# 4

maternites <- st_read(dsn = conn, query = "SELECT REG, COUNT(ID) FROM bpe21_metro WHERE TYPEQU='D107' GROUP BY REG")

# 5a

cinema_bpe <- st_read(dsn = conn, query = "SELECT * FROM bpe21_metro WHERE TYPEQU='F303'")

# 5b

# On construit un buffer de 1km (une zone tampon) autour de la sorbonne
# df des coordonnées
sorbonne_buffer <- data.frame(x=2.34297,y=48.84864) %>% 
  #qu'on transforme en objet sf (systeme de proj WGS84 => crs=4326)
  st_as_sf(coords = c("x","y"), crs = 4326) %>% 
  # on reprojette en LAMBERT-93 (crs=2154)
  st_transform(2154) %>% 
  # on crée la zone tampon autour du point (l'unité est le mètre ici)
  st_buffer(1000) 

str(sorbonne_buffer) # le buffer est constitué d'un unique polygône
plot(sorbonne_buffer %>% st_geometry()) # qui s'avère être un cercle

# 5c

cinemas_buffer <- st_within(cinema_bpe, sorbonne_buffer)
cinemas_buffer_dataframe <- data.frame(cinemas_buffer)

cinemas_sorbonne <- cinema_bpe[cinemas_buffer_dataframe$row.id,]

# 6

library(leaflet)
# Optionnel :
# On récupère une icone spécifique sur https://ionic.io/ionicons (mot clé film)
cinemaIcons <- makeIcon(iconUrl = "film.png", 18,18)

leaflet() %>% 
  setView(lat = 48.84864, lng = 2.34297, zoom = 15) %>% 
  addTiles() %>% 
  addMarkers(lat = 48.84864, lng = 2.34297) %>% 
  addCircles(
    lat = 48.84864, lng = 2.34297, weight = 1, radius = 1000
  ) %>% 
  addMarkers(data = cinemas_sorbonne %>% st_transform(4326), icon = cinemaIcons)