library(dplyr)
library(sf)
library(mapsf)
library(classInt)
library(leaflet)

# 1

pop19 <- readxl::read_xlsx("Pop_legales_2019.xlsx") %>% 
  mutate(
    COM = ifelse(substr(COM,1,2) == 75, "75056", COM)
  ) %>% 
  group_by(COM) %>% 
  summarise(PMUN19 = sum(PMUN19), .groups="drop")

str(pop19)

metro_sf <- st_read("fonds/commune_francemetro_2021.shp",
                    options="ENCODING=WINDOWS-1252") %>% 
  left_join(pop19,by=c("code"="COM")) %>% 
  mutate(
    DENSITE=PMUN19/surf
  )

str(metro_sf)

# 2
summary(metro_sf$DENSITE)
plot(metro_sf$DENSITE)
barplot(metro_sf$DENSITE)

# 3

plot(metro_sf["DENSITE"], border=FALSE)

# 4

#plot(metro_sf["DENSITE"], border=FALSE, breaks="jenks")
plot(metro_sf["DENSITE"], border=FALSE, breaks="quantile")
plot(metro_sf["DENSITE"], border=FALSE, breaks="sd")
plot(metro_sf["DENSITE"], border=FALSE, breaks="pretty")
plot(metro_sf["DENSITE"], border=FALSE, breaks="fisher")

# 5a

class_densite_quantile <- classIntervals(
  metro_sf$DENSITE,
  style = "quantile",
  n = 5
)

str(class_densite_quantile)
class_densite_quantile$brks

# 5b

pal1 <- RColorBrewer::brewer.pal(n = 5, name = "YlOrRd")

plot(class_densite_quantile, pal=pal1,main="Un joli graphique (Quantile)")

# 5c

class_densite_jenks <- classIntervals(
  metro_sf$DENSITE,
  style = "jenks",
  n = 5
)

plot(class_densite_jenks, pal=pal1, main="Un joli graphique (Jenks)")

class_densite_sd <- classIntervals(
  metro_sf$DENSITE,
  style = "sd",
  n = 5
)

plot(class_densite_sd, pal=pal1, main="Un joli graphique (SD)")

class_densite_pretty <- classIntervals(
  metro_sf$DENSITE,
  style = "pretty",
  n = 5
)

plot(class_densite_pretty, pal=pal1, main="Un joli graphique (pretty)")

# 5d

densite_disc <- cut(metro_sf$DENSITE,breaks=c(0,40,162,1000,8000,27200),
                    include.lowest=TRUE, right=FALSE)

# 5e

plot(densite_disc)
summary(densite_disc)

plot(metro_sf["DENSITE"], border=FALSE, breaks=c(0,40,162,1000,8000,27200), pal=pal1)