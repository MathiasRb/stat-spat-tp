library(dplyr)
library(sf)
library(mapsf)
library(classInt)
library(leaflet)

# 1
pauv18 <- readxl::read_xlsx("Taux_pauvrete_2018.xlsx")

pauv18 <- pauv18 %>% 
  filter(!Code %in% c("971", "972", "973", "974", "976"))



metro_pv <- st_read("fonds/dep_francemetro_2021.shp",
                    options="ENCODING=WINDOWS-1252") %>% 
  left_join(pauv18,by=c("code"="Code"))

mf_map(x=metro_pv, var = "Tx_pauvrete", type = "choro", breaks = "fisher")
mf_map(x=metro_pv, var = "Tx_pauvrete", type = "choro", breaks = "equal")
mf_map(x=metro_pv, var = "Tx_pauvrete", type = "choro", breaks = "quantile")

# 2

breaks_pauvrete = c(0, 13, 17, 25, max(pauv18$Tx_pauvrete))

metro_pv_paris <- metro_pv %>% 
  filter(code %in% c("75", "92", "93", "94"))

mf_inset_off()
mf_map(x=metro_pv, var = "Tx_pauvrete", type = "choro", breaks = breaks_pauvrete)
mf_map(x=metro_pv_paris, add = T)
mf_inset_on(x = metro_pv_paris, pos = "topright", cex = .25)
mf_init(metro_pv_paris)
mf_map(metro_pv, add = T)
mf_map(metro_pv_paris, add = T,  leg_pos=NA, var = "Tx_pauvrete", type = "choro", breaks = breaks_pauvrete)
mf_inset_off()
mf_layout()

# 3

sf::st_write(metro_pv, "dept_tx_pauvrete_2018.gpkg")
