# 1)
require(sf)

#setwd("./fonds")
carte <- st_read('commune_francemetro_2021.shp', options="ENCODING=WINDOWS-1252")

# 2)

str(carte)
summary(carte)

# 3)

View(carte[1:10,])

# 4)

st_crs(carte)

# 5)

require(dplyr)
communes_Bretagne <- carte %>% 
  filter(dep %in% c(29,56,35,22)) %>% 
  select(code,libelle,epc,dep,surf)

# 6)
str(communes_Bretagne) #oui

# 7)

plot(communes_Bretagne, lwd = 0.1)

# 8)

plot(communes_Bretagne, lwd = 0.1)

# 9)

communes_Bretagne$surf2 <- st_area(communes_Bretagne$geometry) # m²

# 10)

communes_Bretagne$surf2km2 <- communes_Bretagne$surf2/1000000
units(communes_Bretagne$surf2km2) <- "km^2"

# 11) Problème d'unité

# 12) 


Bre2 <- communes_Bretagne %>% 
  select(dep,surf2km2) %>% 
  group_by(dep) %>% 
  summarise(surf2km2 = sum(surf2km2))

plot(Bre2)

# 13)

depBretagne <- Bre2 %>% 
  group_by(dep) %>% 
  summarise(union = st_union(geometry))

plot(depBretagne, main="Départements bretons")

# 14)

centroid_dept_bretagne <- st_centroid(depBretagne$union)
# a) C'est un point

# b)

plot(st_geometry(depBretagne))
plot(centroid_dept_bretagne, add=TRUE)

# c)

noms_deps <- c("Côtes d'Armor","Finistère","Ille-et-Vilaine","Morbihan")

centr <- cbind(centroid_dept_bretagne, noms_deps)

# d)

centroid_coords <- st_drop_geometry(st_coordinates(centroid_dept_bretagne))

bind_cols(centroid_coords, noms_deps, depBretagne$dep)


# e)

plot(st_geometry(depBretagne))
plot(centroid_dept_bretagne, add=TRUE)
text(centroid_coords,labels = noms_deps, pos=1)


# 15)
st_intersects(centroid_dept_bretagne,communes_Bretagne)

communes_Bretagne$libelle[c(148,476,647,1092)]

# 16)
intersect <- st_intersection(centroid_dept_bretagne,communes_Bretagne)
intersectwithin <- st_within(centroid_dept_bretagne,communes_Bretagne)
communes_Bretagne$libelle[c(148,476,647,1092)]

# 17)

pref22 <- communes_Bretagne$geometry[communes_Bretagne$libelle == "Saint-Brieuc"]
pref29 <- communes_Bretagne$geometry[communes_Bretagne$libelle == "Quimper"]
pref56 <- communes_Bretagne$geometry[communes_Bretagne$libelle == "Vannes"]
pref35 <- communes_Bretagne$geometry[communes_Bretagne$libelle == "Rennes"]

st_distance(pref22, centroid_dept_bretagne[1])
st_distance(pref29, centroid_dept_bretagne[2])
st_distance(pref35, centroid_dept_bretagne[3])
st_distance(pref56, centroid_dept_bretagne[4])

# 18)

#a) 
zone <- st_buffer(centroid_dept_bretagne, 20000)

#b)

plot(zone, add=TRUE)

#c)

communes_dans_zones <- st_intersection(zone, communes_Bretagne$geometry)

#d)

plot(communes_dans_zones, add=TRUE)
length(communes_dans_zones)

# 19)
# a)

transforme <- st_transform(communes_Bretagne$geometry, crs=st_crs("WGS84"))

#b) 

plot(transforme)

# 20)

communes_Bretagne$surf3 <- st_area(transforme)
