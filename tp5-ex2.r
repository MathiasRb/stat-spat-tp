# 1

library(DBI)
source(file = "connexion_db.R")
conn<-connecter()
DBI::dbListTables(conn)

# 2

DBI::dbListFields(conn,"popnaiss_com")

# 3

sql_popnaisscom <- dbSendQuery(conn = conn, statement = "SELECT * FROM popnaiss_com")
 
# 4

sql_popnaisscom <- dbGetQuery(conn = conn, statement = "SELECT * FROM popnaiss_com")

# 5

sql_popnaisscom_Rennes <- dbGetQuery(conn = conn, statement = "SELECT * FROM popnaiss_com WHERE codgeo = '35238'")

# 6

sql_popnaisscom_Bruz <- dbGetQuery(conn = conn, statement = "SELECT * FROM popnaiss_com INNER JOIN bpe21_metro ON popnaiss_com.codgeo = bpe21_metro.depcom WHERE CODGEO = '35047'")

# 7a

library(dplyr)
library(dbplyr)

# Connexion à la table popnaiss
popnaiss<-tbl(conn,"popnaiss_com")
str(popnaiss) # ! ce n'est pas un data.frame

# Reprise de la question 5
popnaiss %>% 
  filter(codgeo=="35047") %>% 
  show_query()

pop_bruz <- popnaiss %>% 
  filter(codgeo=="35047") %>% 
  collect()
str(pop_bruz)

# show_query montre la requete
# collect collecte les données

# 7b

bpe21_metro <-tbl(conn,"bpe21_metro")

jointure <- bpe21_metro %>% 
  filter(depcom == "35047") %>%
  inner_join(popnaiss, by=c("depcom" = "codgeo")) %>% 
  show_query()
